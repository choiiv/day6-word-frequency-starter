import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String WHITESPACE_PATTERN = "\\s+";
    public static final String LINE_BREAK = "\n";

    public String getResult(String sentence) {
        try {
            List<Input> inputList = Count(sentence);
            return inputList.stream()
                    .sorted((firstWord, secondWord) -> secondWord.getWordCount() - firstWord.getWordCount())
                    .map(input -> input.getValue() + " " + input.getWordCount())
                    .collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private List<Input> Count(String sentence) {
        List<String> words = List.of(sentence.split(WHITESPACE_PATTERN));
        HashSet<String> distinctWords = new HashSet<>(words);
        return distinctWords.stream()
                .map(word -> new Input(word, Collections.frequency(words, word)))
                .collect(Collectors.toList());
    }
}
